## Full info at the website : ##
[http://nullmailer-install-daemonize.bitbucket.io](http://nullmailer-install-daemonize.bitbucket.io "Nullmailer - Installer - Daemonizer - Logger").
=====================================

This is a publish only repository. Pull requests and issues will be ignored.
But you can comment and ask questions at the website above.

(And no, you cannot daemonize bitbucket.org.)
