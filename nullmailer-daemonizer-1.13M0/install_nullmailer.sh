#!/bin/bash
# ========================================================================================================================================== #
# The MIT License (MIT)
# Copyright (c) <2012> <Robertino Waarde>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ========================================================================================================================================== #
# Script name    : install_nullmailer.sh.
# Script version : 1.13M0.
# OS             : Debian NetInst / Ubuntu Server.
# Programmer     : Robertino Waarde.
# ęCopyright     : Robertino Waarde.
# Date           : August 2012.
# Run with       : cd into the directory with this script, and run with : ./install_nullmailer.sh
# ========================================================================================================================================== #
# Description :
# Downloads nullmailer v1.13 source code from the official website.
# Patches nullmailer-send to redirect its stdout and stderr to one or two files.
# Compiles nullmailer.
# Daemonizes nullmailer with start/stop init.d script.
# Works with Gmail, and Gmail in Google Apps. Yeah baby!
# ========================================================================================================================================== #
# Assuming :
# root access.
# You uninstalled other mailers like sendmail and postfix. (nullmailer installs a sendmail emulator called sendmail.)
# TIP : Uninstall by leaving your config files in place. Use "apt-get remove" instead of "apt-get purge".
# Tested on a VPS in an OpenVZ node.
# Debian NetInst or Ubuntu Server Edition has been installed from the VPS Control Panel.
# A Bash shell. Tested with GNU bash, version 4.2.24(1)-release (x86_64-pc-linux-gnu).
# ========================================================================================================================================== #

# Is root running this script ?                    THIS CHECK SHOULD ALWAYS BE THE FIRST CODE THAT'S EXECUTED.
if [ "`id -u`" -ne 0 ]
then
  echo -e "\n\nRun this script as root!\n\n"
  exit -1
fi

# Constant and variable definitions.
readonly defaultInstallDir="/usr/local"          # The nullmailer default. Can't be empty.
readonly defaultConfigDir="/etc"                 # The nullmailer default. Can't be empty.
readonly defaultLogDir="/var/log/nullmailer"     # This installer's default. Can't be empty.

readonly cNULLMAILERINSTALLER=`pwd -P`             # Grab the directory path where we are installing from. All nullmailer-installer uploads must be in this directory.
readonly cNULLMAILERSCRATCHDIR="/tmp/nullmailer" # Temporary directory for downloading, patching, and compiling nullmailer. Can't be empty.
readonly cNULLMAILERLOGDIR=$defaultLogDir        # Directory where nullmailer-send's redirected stdout and stderr log files will be stored. Change as required. Can't be empty.
readonly cNULLMAILERROOT=$defaultInstallDir      # The nullmailer default is "/usr/local". Change as required. Can't be empty.
readonly cNULLMAILERCONFIG=$defaultConfigDir     # The nullmailer default is "/etc". Change as required. Can't be empty.
readonly cNULLMAILERUSER="nullmail"              # nullmailer expects this user to exist. At end of compilation it sets ownership and permissions for this user. Can't be empty.
readonly cNULLMAILERGROUP="nullmail"             # May be any existing group you like. Can't be empty.
readonly cNULLMAILERVERSION="1.13"               # Change this for a newer/older version. A newer version also requires a new patched send.cc file and uninstaller. Can't be empty.

readonly cTRUE=0
readonly cFALSE= # Assigns NULL value for boolean testing.

readonly cTPUT="/usr/bin/tput"
readonly cT_B=`$cTPUT bold`
readonly cT_R=`$cTPUT rev`
readonly cT_NORMAL=`$cTPUT sgr0`

# Simplify frequently used tests.
defaultInstallDirUsed=$cFALSE
defaultConfigDirUsed=$cFALSE
defaultLogDirUsed=$cFALSE
[ "$cNULLMAILERROOT" == "$defaultInstallDir" ] && defaultInstallDirUsed=$cTRUE
[ "$cNULLMAILERCONFIG" == "$defaultConfigDir" ] && defaultConfigDirUsed=$cTRUE
[ "$cNULLMAILERLOGDIR" == "$defaultLogDir" ] && defaultLogDirUsed=$cTRUE

# Only r+w for owner root because of login credentials in the file.
/bin/chown -v 0:0 "$cNULLMAILERINSTALLER/configFiles/remotes.prefab"
/bin/chmod -vR 600 "$cNULLMAILERINSTALLER/configFiles/remotes.prefab"

# Update the list of packages. Install linkers, compilers, etc. And also TLS dev libs.
/usr/bin/apt-get update
/usr/bin/apt-get install build-essential
/usr/bin/apt-get install libgnutls-dev

# Create the user and group for nullmailer. (Can't login, and has no home directory.)
/usr/sbin/addgroup $cNULLMAILERGROUP
/usr/sbin/useradd --shell /usr/sbin/nologin -M $cNULLMAILERUSER -g $cNULLMAILERGROUP

# Make some required directories :
#   make+install will set owner+mode to+for user $cNULLMAILERUSER where necessary in $cNULLMAILERROOT.
[ ! "$defaultInstallDirUsed" ] && /bin/mkdir -vp "$cNULLMAILERROOT"
#   Create the directory to hold the nullmailer conf files, and make it accessible to user $cNULLMAILERUSER.
#/bin/mkdir -vp "$cNULLMAILERCONFIG/nullmailer"; /bin/chown -v $cNULLMAILERUSER:0 "$cNULLMAILERCONFIG/nullmailer"; /bin/chmod -v 640 "$cNULLMAILERCONFIG/nullmailer";
#   nullmaler-send will run as a non-privileged user, so it can't make directories+files in /var/log.
/bin/mkdir -vp "$cNULLMAILERLOGDIR"; /bin/chown -v nullmail:0 "$cNULLMAILERLOGDIR"; /bin/chmod -v 755 "$cNULLMAILERLOGDIR";
#   Throwaway directory to build nullmailer.
/bin/mkdir -vp "$cNULLMAILERSCRATCHDIR"

# The following table lists all the control files used by nullmailer.
#              control file        used by
#              ------------------- -----------------
#              me                  nullmailer-send, nullmailer-inject; FQDN e.g. server01.mydomainname.com or mydomainname.com of the server running nullmailer.
#              remotes             nullmailer-send; One or more smart mailservers to send the email message to. E.g. smtp.gmail.com smtp --port=587 --auth-login --starttls --insecure --user=someone@gmail.com --pass=YourPasswordHere
#              adminaddr           nullmailer-queue; Recipient email address will be written to this email address when e.g. the to-email-address is @domainname in file me, or @localhost.
#              defaulthost         nullmailer-inject; Your server's name e.g. server01 or server01.mydomainname.com.
#              defaultdomain       nullmailer-inject; Domain name e.g. mydomainname.com. Postpended to defaulthost if the name in file defaulthost conatins no dots.
#              idhost              nullmailer-inject; Used when building the message-id string for the message. Defaults to the canonicalized value of defaulthost. If not found, value of file me.
#              helohost            nullmailer-send; Value for envvar HELOHOST. Should contain a domainname to identify us with, with the HELO/EHLO SMTP-command, at the remote mailserver.
#              pausetime           nullmailer-send; Time (in seconds) to before re-checking the queue. 0=Exit after first check : one-shot-mode. If not found, 60 seconds.
#              sendtimeout         nullmailer-send; Time (in seconds) to wait for completion of sending one email message, before retrying. 0=wait forever. If not found, 60 x 60 seconds. Yes, 1 hour!
#
#              envvar              used by
#              ------------------- -----------------
#              HELOHOST            nullmailer-send; Should contain a domainname to identify us with, with the HELO/EHLO SMTP-command, at the remote mailserver. Value of file helohost, or if file helohost is not found the value of file me.

# Download and install nullmailer.
cd $cNULLMAILERSCRATCHDIR
/usr/bin/wget "http://untroubled.org/nullmailer/nullmailer-$cNULLMAILERVERSION.tar.gz"
tar -zxvf "nullmailer-$cNULLMAILERVERSION.tar.gz"
cd "nullmailer-$cNULLMAILERVERSION"

echo -e "\nOverwriting the original nullmailer-send source file (send.cc), with the modified version.\n"
/bin/cp -vf "$cNULLMAILERINSTALLER/send.cc" src/

# Specify the correct configuration options.
# For more configuration options : '/tmp/nullmailer/nullmailer-1.13/configure --help'.
if     [ "$defaultInstallDirUsed" ] && [ "$defaultConfigDirUsed" ]
then
    echo -e "\nSpecifying : ./configure --enable-tls\n"
    ./configure --enable-tls
fi

if     [ "$defaultInstallDirUsed" ] && [ ! "$defaultConfigDirUsed" ]
then
    echo -e "\nSpecifying : ./configure --sysconfdir=$cNULLMAILERCONFIG --enable-tls\n"
    ./configure --sysconfdir=$cNULLMAILERCONFIG --enable-tls
fi

if     [ ! "$defaultInstallDirUsed" ] && [ "$defaultConfigDirUsed" ]
then
    echo -e "\nSpecifying : ./configure --sysconfdir=$cNULLMAILERCONFIG --enable-tls\n"
    ./configure --prefix=$cNULLMAILERROOT --enable-tls
fi

if     [ ! "$defaultInstallDirUsed" ] && [ ! "$defaultConfigDirUsed" ]
then
    echo -e "\nSpecifying : ./configure --sysconfdir=$cNULLMAILERCONFIG --enable-tls\n"
    ./configure --prefix=$cNULLMAILERROOT --sysconfdir=$cNULLMAILERCONFIG --enable-tls
fi

make
make install install-root

# Preserve the nullmailer flow diagram.
find "$cNULLMAILERSCRATCHDIR/nullmailer-$cNULLMAILERVERSION" -type f -iname diagram -exec /bin/cp -vf {} "$cNULLMAILERINSTALLER/diagram" \; # Case insensitive search, so specify a target filename on the 'cp'-cmd.

cd $cNULLMAILERINSTALLER

# The nullmailer installer did some funky stuff by combining '--prefix' and '--sysconfdir', to make the directory where it expects its configuration files.
# We are going to take the easy way out, and lookup the end result.
# So rescue all the full paths defined, during the build, in source file 'defines.cc'. DO NOT DELETE THIS FILE. It is needed by the uninstaller too.
# We are looking for the path where nullmailer expects its configuration files (CONFIG_DIR).
echo -e "\nLooking for the installation path definitions.\n"
nullmailerConfigMsg=""
find "$cNULLMAILERSCRATCHDIR/nullmailer-$cNULLMAILERVERSION" -type f -iname defines.cc -exec /bin/cp -vf {} "$cNULLMAILERINSTALLER/defines.cc" \; # Case insensitive search, so specify a target filename on the 'cp'-cmd.
if    [ -e "$cNULLMAILERINSTALLER/defines.cc" ]\
   && [ -s "$cNULLMAILERINSTALLER/defines.cc" ]
then
    echo -e "^^ Found.\n"
    nullmailerConfigMsg="\n\nSource file "$cT_B"'defines.cc'"$cT_NORMAL" was copied to the installation directory. "$cT_B$cT_R"Do NOT delete this file."$cT_NORMAL" It is required by the uninstall script.\n\n"
    CONFIG_DIR=`sed -rn s'-.*CONFIG_DIR="(.*)/";-\1-p' <$cNULLMAILERINSTALLER/defines.cc` # Nullmailer's default = '/usr/local/etc/nullmailer'.
else
    echo -e "Not Found.\n"
    nullmailerConfigMsg=$cT_B$cT_R"The C++ file 'defines.cc' which contains the installation paths was not found. All config files were created in $cNULLMAILERROOT/nullmailerconfigs."$cT_NORMAL
    /bin/mkdir "$cNULLMAILERROOT/nullmailerconfigs" # We shouldn't get here.
    CONFIG_DIR="$cNULLMAILERROOT/nullmailerconfigs"
fi

echo -e "\nCleaning up build files.\n"
rm -vfR $cNULLMAILERSCRATCHDIR

# Add the nullmailer config files to SYSCONFDIR/nullmailer/, e.g. SYSCONFDIR/nullmailer/me, SYSCONFDIR/nullmailer/remotes, etc.
# nullmailer always looks for its config files in a directory 'nullmailer', within SYSCONFDIR. (See Makefile.in, search for '@sysconfdir@/nullmailer')
if [ -n CONFIG_DIR ]
then
    echo -e "\nConfiguring.\n"
    /bin/hostname >"$CONFIG_DIR/me"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/me"
    /bin/cp -vf "$cNULLMAILERINSTALLER/configFiles/remotes.prefab" "$CONFIG_DIR/remotes"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/remotes"
    /bin/chmod -v 600 "$CONFIG_DIR/remotes" # Only r+w for owner because of login credentials in file.
    /bin/cp -vf "$cNULLMAILERINSTALLER/configFiles/adminaddr.prefab" "$CONFIG_DIR/adminaddr"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/adminaddr"
    /bin/hostname >"$CONFIG_DIR/defaulthost" # Or e.g. : echo "server1" >"$CONFIG_DIR/defaulthost"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/defaulthost"
    /bin/cp -vf "$cNULLMAILERINSTALLER/configFiles/defaultdomain.prefab" "$CONFIG_DIR/defaultdomain"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/defaultdomain"
    /bin/cp -vf "$cNULLMAILERINSTALLER/configFiles/idhost.prefab" "$CONFIG_DIR/idhost"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/idhost"
    /bin/hostname >"$CONFIG_DIR/helohost"
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/helohost"
    /bin/cp -vf "$cNULLMAILERINSTALLER/configFiles/pausetime.prefab" "$CONFIG_DIR/pausetime" # 60 seconds between attempts to check the email queue.
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/pausetime"
    /bin/cp -vf "$cNULLMAILERINSTALLER/configFiles/sendtimeout.prefab" "$CONFIG_DIR/sendtimeout" # 3600 seconds before transmission of an email message is cancelled.
    /bin/chown -v $cNULLMAILERUSER:0 "$CONFIG_DIR/sendtimeout"
fi

# Add the start/stop script to daemonize nullmailer-send at every boot, and so that we can control it with "service nullmailer-send {start|stop|status|restart|force-reload|check}"
echo -e "\nAutomating.\n"
/bin/cp -vf "$cNULLMAILERINSTALLER/--etc--init.d--/nullmailer-send.prefab" /etc/init.d/nullmailer-send
/bin/chown -vR 0:0 /etc/init.d/nullmailer-send; /bin/chmod -v 755 /etc/init.d/nullmailer-send
/usr/sbin/update-rc.d nullmailer-send defaults # Remove with : 'update-rc.d -f nullmailer-send remove'.

# Update locate's database.
echo -e "\nMaking the following nullmailer objects locatable :\n"
/usr/bin/updatedb -v | egrep 'nullmail|mailq|sendmail'

# Add "man nullmailer", "man sendmail", "man nullmailer-inject", "man nullmailer-queue", "man nullmailer-send".
# Man pages are usually stored in '/usr/local/share/man/man*' or '/usr/share/man/man*'. So, if nullmailer was installed to '/usr/local' or '/usr' they would already be in place.
echo -e "\nMaking the nullmailer man pages available.\n"
[ ! "$defaultInstallDirUsed" ] && [ $cNULLMAILERROOT != "/usr" ] && /bin/cp -vfR "$cNULLMAILERROOT/share/man/*" /usr/local/share/man
/usr/bin/mandb    # '/usr/local/share/man' is (like '/usr/share/man') one of the processing paths of mandb.

# /usr/bin/apt-get purge libgnutls-dev # Optional
# /usr/bin/apt-get purge build-essential # Optional
# /usr/bin/apt-get autoremove; apt-get autoclean # Optional

# Fire up nullmailer-send as a daemon.
echo -e "\nStarting up the nullmailer-send daemon.\n"
service nullmailer-send start

# Display the daemon's (nullmailer-send) process.
ps aux | egrep null | egrep -v 'egrep|install_nullmailer.sh'

# Display the daemon's (nullmailer-send) running status.
service nullmailer-send status

# Display the message to hold on to CPP source file 'defines.cc'. (Or that it could not be found).
echo -e $nullmailerConfigMsg

# Done (((((Hunny)))).
exit 0
