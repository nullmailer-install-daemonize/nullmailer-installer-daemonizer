#!/bin/bash
# ========================================================================================================================================== #
# The MIT License (MIT)
# Copyright (c) <2012> <Robertino Waarde>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ========================================================================================================================================== #
# Script name    : uninstall_nullmailer.sh.
# Script version : 1.13M0.
# OS             : Debian Small / Ubuntu Server.
# Programmer     : Robertino Waarde.
# ęCopyright     : Robertino Waarde.
# Date           : August 2012.
# Run with       : cd into the directory with this script, and run with : ./uninstall_nullmailer.sh
# ========================================================================================================================================== #
# Description :
# Removes nullmailer v1.13  completely.
# ========================================================================================================================================== #
# Assuming :
# nullmailer was previously installed with the installer script install_nullmailer.sh.
# Matching versions of nullmailer installer and uninstaller.
# Matching nullmailer version number in installer and uninstaller scripts.
# root access.
# Tested on a VPS in an OpenVZ node.
# Debian Small or Ubuntu Server Edition.
# A Bash shell. Tested with GNU bash, version 4.2.24(1)-release (x86_64-pc-linux-gnu).
# ========================================================================================================================================== #

# Is root running this script ?                    THIS CHECK SHOULD ALWAYS BE THE FIRST CODE THAT'S EXECUTED.
if [ "`id -u`" -ne 0 ]
then
  echo -e "\n\nRun this script as root!\n\n"
  exit -1
fi

# Constant and variable definitions.
readonly cNULLMAILERINSTALLER=`pwd -P`           # Grab the directory path where we installed from. All nullmailer installer uploads and source file defines.cc, must be in this directory.

readonly cNULLMAILERROOT=`sed -rn s'-.*BIN_DIR="(.*)/bin/";-\1-p' <$cNULLMAILERINSTALLER/defines.cc` # The directory into which nullmailer was installed. Can't be empty.
readonly cNULLMAILERCONFIG=`sed -rn s'-.*CONFIG_DIR="(.*)/";-\1-p' <$cNULLMAILERINSTALLER/defines.cc` # The directory into which nullmailer's config files were installed. Can't be empty.
readonly cNULLMAILERSCRATCHDIR="/run/nullmailer-send" # Temporary directory for downloading, patching, and compiling nullmailer. Can't be empty.
readonly cNULLMAILERLOGDIR="/var/log/nullmailer" # Directory where nullmailer-send's redirected stdout and stderr log files will be stored. Change as required. Can't be empty.
readonly cNULLMAILERUSER="nullmail"              # nullmailer expects this user to exist. At end of compilation it sets ownership and permissions for this user. Can't be empty.
readonly cNULLMAILERGROUP="nullmail"             # May be any existing group you like. Can't be empty.
readonly cNULLMAILERVERSION="1.13"               # Not used by the uninstaller, but defined here to indicate which nullmailer version is compatible. Can't be empty.

# Stop the nullmailer-send daemon.
echo -e "\nStopping the nullmailer-send daemon."
service nullmailer-send stop

# Remove the nullmailer-send daemon startup entries (from init.d).
echo -e "\nRemoving the nullmailer-send daemon startup entries."
update-rc.d -f nullmailer-send remove

# Remove nullmailer-send start/stop daemon script.
echo -e "\nRemoving the nullmailer-send daemon start/stop script."
rm --preserve-root -vf /etc/init.d/nullmailer-send

# Remove nullmailer-send scratchpad directory.
# /run is not actually a file system, and anything in it shouldn't survive a reboot.
# But this is Linux, so the next reboot might be a century away. So we still delete from here.
echo -e "\nRemoving the nullmailer-send scratchpad and PID file directory."
rm --preserve-root -vfR $cNULLMAILERSCRATCHDIR

# Remove log file directory.
echo -e "\nRemoving nullmailer's logging directory and log files."
rm -vfR --preserve-root $cNULLMAILERLOGDIR

# Remove nullmailer's binaries, queue, man pages, etc :
echo -e "\nRemoving nullmailer's binaries, queue, man pages, config files, etc."
rm --preserve-root -vf $cNULLMAILERROOT/bin/mailq
rm --preserve-root -vf $cNULLMAILERROOT/bin/nullmailer-inject
rm --preserve-root -vf $cNULLMAILERROOT/bin/nullmailer-smtpd
rm --preserve-root -vfR $cNULLMAILERROOT/libexec/nullmailer
rm --preserve-root -vf $cNULLMAILERROOT/sbin/nullmailer-queue
rm --preserve-root -vf $cNULLMAILERROOT/sbin/nullmailer-send
rm --preserve-root -vf $cNULLMAILERROOT/sbin/sendmail
rm --preserve-root -vf $cNULLMAILERROOT/share/man/man1/nullmailer-inject.1
rm --preserve-root -vf $cNULLMAILERROOT/share/man/man1/sendmail.1
rm --preserve-root -vf $cNULLMAILERROOT/share/man/man7/nullmailer.7
rm --preserve-root -vf $cNULLMAILERROOT/share/man/man8/nullmailer-queue.8
rm --preserve-root -vf $cNULLMAILERROOT/share/man/man8/nullmailer-send.8
rm --preserve-root -vfR $cNULLMAILERROOT/var/nullmailer
rm --preserve-root -vfR $cNULLMAILERCONFIG

# Remove user nullmail :
echo -e "\nRemoving nullmailer's user profile ( $cNULLMAILERUSER )."
# First lock the password. (Just in case the deluser fails.)
/usr/bin/passwd -l $cNULLMAILERUSER
# Then change the user's shell to something not executable. (Just in case the deluser fails.)
chsh -s /dev/null $cNULLMAILERUSER
# Now delete the user completely from the system.
echo -e "\nJust ignore messages like '/usr/sbin/deluser: Cannot handle special file '"
echo -e "It's retarded 'deluser' poking its nose in directories where it shouldn't, and then whinging that it cannot handle it."
/usr/sbin/deluser --quiet --remove-home --remove-all-files $cNULLMAILERUSER

# Remove group nullmail.
echo -e "\nRemoving nullmailer's group ( $cNULLMAILERGROUP ) when empty."
/usr/sbin/delgroup --only-if-empty $cNULLMAILERGROUP

# Uninstall linkers, compilers, etc. And also uninstall GnuTLS dev libs.
# apt-get purge libgnutls-dev # Optional
# apt-get purge build-essential # Optional
# apt-get autoremove; apt-get autoclean # Optional

# Update man's database :
echo -e "\nUpdating the man command's database."
/usr/bin/mandb

# Update locate's database :
echo -e "\nUpdating the locate command's database."
echo -e "    Remaining nullmailer objects :"
/usr/bin/updatedb -v | egrep 'nullmail|mailq|sendmail'
echo -e "    (If all is OK : none, or only in the directory with your install scripts.)"

# We're done.
echo -e "\nnullmailer was removed from your system.\n"

exit 0
