// nullmailer -- a simple relay-only MTA
// Copyright (C) 2012  Bruce Guenter <bruce@untroubled.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// You can contact me at <bruce@untroubled.org>.  There is also a mailing list
// available to discuss this package.  To subscribe, send an email to
// <nullmailer-subscribe@lists.untroubled.org>.

#include "config.h"
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "ac/time.h"
#include "configio.h"
#include "defines.h"
#include "errcodes.h"
#include "fdbuf/fdbuf.h"
#include "hostname.h"
#include "itoa.h"
#include "list.h"
#include "selfpipe.h"
#include "setenv.h"

// begin RW001
#include <cstdio>
#include <getopt.h>
#include <iostream>

void display_usage( std::string thisProgram )
{

    const std::string cNORMAL      ("\033[0m");
    const std::string cBOLD        ("\033[1m");
    const std::string cITALIC      ("\033[3m");
    const std::string cUNDERLINE   ("\033[4m");
    const std::string cREVERSE     ("\033[7m");
    const std::string cBLACK       ("\033[30m");
    const std::string cRED         ("\033[31m");
    const std::string cGREEN       ("\033[32m");
    const std::string cYELLOW      ("\033[33m");
    const std::string cBLUE        ("\033[34m");
    const std::string cMAGENTA     ("\033[35m");
    const std::string cCYAN        ("\033[36m");
    const std::string cWHITE       ("\033[37m");

    std::cout
         << cBOLD << "NAME" << cNORMAL
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " Version " << VERSION <<" - The transmission part of " << PACKAGE << ", a simple relay-only mail transport agent"
         << "\n"
         << "\n" << cBOLD << "SYNOPSIS" << cNORMAL
         << "\n\t" << cBOLD << thisProgram << cNORMAL <<" [option...] [value]"
         << "\n"
         << "\n" << cBOLD << "DESCRIPTION" << cNORMAL
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " Version " << VERSION << " - The transmission part of " << PACKAGE << ", a simple relay-only mail transport agent"
         << "\n\tReads email messages from its queue at 'INSTALL_DIR/var/nullmailer/queue/'. Default INSTALL_DIR is /usr/local."
         << "\n\tThese email messages were queued by nullmailer-queue, through nullmailer-inject or sendmail (the emulator provided by " << PACKAGE << ")."
         << "\n\tEmail messages may be delivered (through various protocols) from the queue to remote 'smart' servers. The SMTP protocol is the default."
         << "\n\tCommand 'man nullmailer-send' for more detailed information."
         << "\n"
         << "\n\t" << cBOLD+cRED << "This is a modified version of " << thisProgram << "." << cNORMAL
         << "\n"
         << "\n" << cBOLD << "CHANGES BY NON COPYRIGHT HOLDERS" << cNORMAL
         << "\n\tName                           Date (MMM-YYYY) Description"
         << "\n\tRobert Waarde                  August 2012       " << cBOLD << thisProgram << cNORMAL << " now accepts command line options to redirect audit and error messages,"
         << "\n\t                                                 from stdout and stderr to 1 or 2 log files."
         << "\n\t                                                 Options added : --logging, -p, --logpath, -m, --msglog, -e, --errlog, --onelog, --help."
         << "\n"
         << "\n" << cBOLD << "OPTIONS" << cNORMAL
         << "\n\t" << cBOLD << "    --logging" << cNORMAL << "            : Switches logging of stdout+stderr to log files on. This option on its own is enough to log to the defaults."
         << "\n\t" << cBOLD << "-p, --logpath " << cNORMAL << cUNDERLINE << "<pathname>" << cNORMAL << " : Full path to the directory where the log file(s) should be created. No trailing slash. Default: '/var/log/nullmailer'."
         << "\n\t" << cBOLD << "-m, --msglog  " << cNORMAL << cUNDERLINE << "<filename>" << cNORMAL << " : Filename for message log (stdout) or combined log (stdout+stderr) (see --onelog option). Default: 'nm-s.msg.log'."
         << "\n\t" << cBOLD << "-e, --errlog  " << cNORMAL << cUNDERLINE << "<filename>" << cNORMAL << " : Filename for error log (stderr). Default: 'nm-s.err.log'."
         << "\n\t" << cBOLD << "    --onelog" << cNORMAL << "             : When specified, indicates that message log and error log will be combined into one log file."
         << "\n\t" << cBOLD << "    --help" << cNORMAL << "               : Display this help and exit."
         << "\n"
         << "\n\tAll command line options are optional. When not specified the defaults will be used."
         << "\n\tWithout any command line option, " << cBOLD << thisProgram << cNORMAL << " will revert to default behavior : No redirection to any file."
         << "\n\t" << cBOLD << "--logging:" << cNORMAL
         << "\n\t\tRequired to turn on logging to log files."
         << "\n\t\tWhen not specified, any other logging options will be ignored."
         << "\n\t" << cBOLD << "--logpath:" << cNORMAL
         << "\n\t\tWhen specified, must contain the path to a directory which already exists."
         << "\n\t\tThis directory must be owned by user nullmail, with the permissions 755 (-rwxr-xr-x)."
         << "\n\t\tWhen " << cUNDERLINE << "not" << cNORMAL << " specified, it defaults to path '/var/log/nullmailer'."
         << "\n\t\tWhich is created by the installer 'install_nullmailer.sh' with owner nullmail and permissions 755 (-rwxr-xr-x)"
         << "\n\t" << cBOLD << "--logpath, --msglog, --errlog:" << cNORMAL
         << "\n\t\tAny change from their default value, before and after installation, should always be reflected to their equivalents in the start/stop daemon script '/etc/init.d/nullmailer-send'."
         << "\n"
         << "\n" << cBOLD << "EXAMPLES" << cNORMAL
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " <without options>"
         << "\n\t    Default behavior. No logging to any file."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --logpath /usr/local/var/log"
         << "\n\t    Log files will be written to directory '/usr/local/var/log'. This directory must exist and owned by user nullmail. See above."
         << "\n\t    Messages to stdout and stderr will be redirected to files named 'nm-s.msg.log' and 'nm-s.err.log' respectively."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --msglog nm-s.audit.log"
         << "\n\t    Messages to stdout will be redirected to log file 'nm-s.audit.log'."
         << "\n\t    Messages to stderr will be redirected to default log file 'nm-s.err.log'."
         << "\n\t    Log files will be written to the default directory '/var/log/nullmailer'."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --msglog nm-s.audit.log --errlog nm-s.error.log"
         << "\n\t    Messages to stdout will be redirected to log file 'nm-s.audit.log'."
         << "\n\t    Messages to stderr will be redirected to log file 'nm-s.error.log'."
         << "\n\t    Log files will be written to the default directory '/var/log/nullmailer'."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --logpath /usr/local/var/log --msglog nm-s.audit.log --errlog nm-s.error.log"
         << "\n\t    Messages to stdout will be redirected to log file 'nm-s.audit.log'."
         << "\n\t    Messages to stderr will be redirected to log file 'nm-s.error.log'."
         << "\n\t    Log files will be written to directory '/usr/local/var/log'."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --onelog"
         << "\n\t    Messages to stdout and stderr will be redirected to one log file, with the default name 'nm-s.msg.log'."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --msglog nm-s.audit.log --onelog"
         << "\n\t    Messages to stdout and stderr will be redirected to one log file, with the name 'nm-s.audit.log'."
         << "\n"
         << "\n\t" << cBOLD << thisProgram << cNORMAL << " --logging --logpath /usr/local/var/log --onelog"
         << "\n\t    Messages to stdout and stderr will be redirected to one log file, with the default name 'nm-s.msg.log'."
         << "\n\t    The log file will be written to directory '/usr/local/var/log'."
         << "\n"
         << "\n" << cBOLD << "COPYRIGHT" << cNORMAL
         << "\n\t" << PACKAGE << ", a sendmail/qmail/etc replacement MTA for hosts which"
         << "\n\trelay to a fixed set of smart relays.  It is designed to be simple to"
         << "\n\tconfigure, secure, and easily extendable."
         << "\n"
         << "\n\tThis package is Copyright(C) 2012 Bruce Guenter, and may be copied"
         << "\n\taccording to the GNU GENERAL PUBLIC LICENSE (GPL) Version 2 or a later"
         << "\n\tversion.  A copy of this license is included with this package.  This"
         << "\n\tpackage comes with no warranty of any kind."
         << std::endl;

    exit( EXIT_FAILURE );

}

struct globalArgs_t
{

    std::string thisProgram;     // The name of this program.
    std::string thisProgramPath; // The path to this program.
    std::string logPath;         // -p --logpath
    std::string msgLogName;      // -m --logmsg
    std::string errLogName;      // -e --logerr
            int logging;         //    --logging
            int oneLog;          //    --onelog
    std::string fullMsgLogPath;
    std::string fullErrLogPath;

} globalArgs;

static const struct option longOpts[] =
{

// struct option { const char *name; int has_arg; int *flag; int val; } in getopt.h.
    { "logpath", required_argument, NULL, 'p' },
    { "msglog",  required_argument, NULL, 'm' },
    { "errlog",  required_argument, NULL, 'e' },
    { "logging", no_argument,       NULL, 0   },
    { "onelog",  no_argument,       NULL, 0   },
    { "help",    no_argument,       NULL, 0   },
    { NULL,      no_argument,       NULL, 0   }

};

static const char *optString = "p:m:e:?";
// end   RW001

selfpipe selfpipe;

typedef list<mystring> slist;

#define fail(MSG) do { fout << MSG << endl; return false; } while(0)
#define fail2(MSG1,MSG2) do{ fout << MSG1 << MSG2 << endl; return false; }while(0)
#define fail_sys(MSG) do{ fout << MSG << strerror(errno) << endl; return false; }while(0)

struct remote
{
  static const mystring default_proto;

  mystring host;
  mystring proto;
  slist options;
  remote(const slist& list);
  ~remote();
};

const mystring remote::default_proto = "smtp";

remote::remote(const slist& lst)
{
  slist::const_iter iter = lst;
  host = *iter;
  ++iter;
  if(!iter)
    proto = default_proto;
  else {
    proto = *iter;
    for(++iter; iter; ++iter)
      options.append(*iter);
  }
}

remote::~remote() { }

typedef list<remote> rlist;

unsigned ws_split(const mystring& str, slist& lst)
{
  lst.empty();
  const char* ptr = str.c_str();
  const char* end = ptr + str.length();
  unsigned count = 0;
  for(;;) {
    while(ptr < end && isspace(*ptr))
      ++ptr;
    const char* start = ptr;
    while(ptr < end && !isspace(*ptr))
      ++ptr;
    if(ptr == start)
      break;
    lst.append(mystring(start, ptr-start));
    ++count;
  }
  return count;
}

static rlist remotes;
static int minpause = 60;
static int pausetime = minpause;
static int maxpause = 24*60*60;
static int sendtimeout = 60*60;

bool load_remotes()
{
  slist rtmp;
  if(!config_readlist("remotes", rtmp) ||
     rtmp.count() == 0)
    return false;
  remotes.empty();
  for(slist::const_iter r(rtmp); r; r++) {
    if((*r)[0] == '#')
      continue;
    slist parts;
    if(!ws_split(*r, parts))
      continue;
    remotes.append(remote(parts));
  }
  return remotes.count() > 0;
}

bool load_config()
{
  mystring hh;
  bool result = true;

  if (!config_read("helohost", hh))
    hh = me;
  setenv("HELOHOST", hh.c_str(), 1);

  if(!load_remotes())
    result = false;

  int oldminpause = minpause;
  if(!config_readint("pausetime", minpause))
    minpause = 60;
  if(!config_readint("maxpause", maxpause))
    maxpause = 24*60*60;
  if(!config_readint("sendtimeout", sendtimeout))
    sendtimeout = 60*60;

  if (minpause != oldminpause)
    pausetime = minpause;

  return result;
}

static slist files;
static bool reload_files = false;

void catch_alrm(int)
{
  signal(SIGALRM, catch_alrm);
  reload_files = true;
}

bool load_files()
{
  reload_files = false;
  fout << "Rescanning queue." << endl;
  DIR* dir = opendir(".");
  if(!dir)
    fail_sys("Cannot open queue directory: ");
  files.empty();
  struct dirent* entry;
  while((entry = readdir(dir)) != 0) {
    const char* name = entry->d_name;
    if(name[0] == '.')
      continue;
    files.append(name);
  }
  closedir(dir);
  return true;
}

void exec_protocol(int fd, remote& remote)
{
  if(close(0) == -1 || dup2(fd, 0) == -1 || close(fd) == -1)
    return;
  mystring program = PROTOCOL_DIR + remote.proto;
  const char* args[3+remote.options.count()];
  unsigned i = 0;
  args[i++] = program.c_str();
  for(slist::const_iter opt(remote.options); opt; opt++)
    args[i++] = strdup((*opt).c_str());
  args[i++] = remote.host.c_str();
  args[i++] = 0;
  execv(args[0], (char**)args);
}

bool catchsender(pid_t pid)
{
  int status;

  for (;;) {
    switch (selfpipe.waitsig(sendtimeout)) {
    case 0:			// timeout
      kill(pid, SIGTERM);
      waitpid(pid, &status, 0);
      selfpipe.waitsig();	// catch the signal from killing the child
      fail("Sending timed out, killing protocol");
    case -1:
      fail_sys("Error waiting for the child signal: ");
    case SIGCHLD:
      break;
    default:
      continue;
    }
    break;
  }

  if(waitpid(pid, &status, 0) == -1)
    fail_sys("Error catching the child process return value: ");
  else {
    if(WIFEXITED(status)) {
      status = WEXITSTATUS(status);
      if(status)
	fail2("Sending failed: ", errorstr(status));
      else {
	fout << "Sent file." << endl;
	return true;
      }
    }
    else
      fail("Sending process crashed or was killed.");
  }
}

bool send_one(mystring filename, remote& remote)
{
  int fd = open(filename.c_str(), O_RDONLY);
  if(fd == -1) {
    fout << "Can't open file '" << filename << "'" << endl;
    return false;
  }
  fout << "Starting delivery: protocol: " << remote.proto
       << " host: " << remote.host
       << " file: " << filename << endl;
  pid_t pid = fork();
  switch(pid) {
  case -1:
    fail_sys("Fork failed: ");
  case 0:
    exec_protocol(fd, remote);
    exit(ERR_EXEC_FAILED);
  default:
    close(fd);
    if(!catchsender(pid))
      return false;
    if(unlink(filename.c_str()) == -1)
      fail_sys("Can't unlink file: ");
  }
  return true;
}

bool send_all()
{
  if(!load_config())
    fail("Could not load the config");
  if(remotes.count() <= 0)
    fail("No remote hosts listed for delivery");
  if(files.count() == 0)
    return true;
  fout << "Starting delivery, "
       << itoa(files.count()) << " message(s) in queue." << endl;
  for(rlist::iter remote(remotes); remote; remote++) {
    slist::iter file(files);
    while(file) {
      if(send_one(*file, *remote))
	files.remove(file);
      else
	file++;
    }
  }
  fout << "Delivery complete, "
       << itoa(files.count()) << " message(s) remain." << endl;
  return true;
}

static int trigger;
#ifdef NAMEDPIPEBUG
static int trigger2;
#endif

bool open_trigger()
{
  trigger = open(QUEUE_TRIGGER, O_RDONLY|O_NONBLOCK);
#ifdef NAMEDPIPEBUG
  trigger2 = open(QUEUE_TRIGGER, O_WRONLY|O_NONBLOCK);
#endif
  if(trigger == -1)
    fail_sys("Could not open trigger file: ");
  return true;
}

bool read_trigger()
{
  if(trigger != -1) {
    char buf[1024];
    read(trigger, buf, sizeof buf);
#ifdef NAMEDPIPEBUG
    close(trigger2);
#endif
    close(trigger);
  }
  return open_trigger();
}

bool do_select()
{
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(trigger, &readfds);
  struct timeval timeout;

  if (files.count() == 0)
    pausetime = maxpause;
  timeout.tv_sec = pausetime;
  timeout.tv_usec = 0;

  pausetime *= 2;
  if (pausetime > maxpause)
    pausetime = maxpause;

  int s = select(trigger+1, &readfds, 0, 0, &timeout);
  if(s == 1) {
    fout << "Trigger pulled." << endl;
    read_trigger();
    reload_files = true;
    pausetime = minpause;
  }
  else if(s == -1 && errno != EINTR)
    fail_sys("Internal error in select: ");
  else if(s == 0)
    reload_files = true;
  if(reload_files)
    load_files();
  return true;
}

// begin RW001
//int main(int, char*[])
//{
int main( int argc, char *argv[] )
{

    FILE* fpMsgLog = NULL;
    FILE* fpErrLog = NULL;
    std::string fqProgramName = argv[0]; // CPPstring = Cstring

    // Initialize to failsafe defaults.
    globalArgs.thisProgram = fqProgramName.substr(fqProgramName.find_last_of("/\\") + 1);
    globalArgs.thisProgramPath = fqProgramName.substr(0,fqProgramName.find_last_of("/\\") - 1);
    globalArgs.logPath = "/var/log/nullmailer";
    globalArgs.msgLogName = "nm-s.msg.log";
    globalArgs.errLogName = "nm-s.err.log";
    globalArgs.logging = 0; // FALSE = No logging to files.
    globalArgs.oneLog = 0; // FALSE = 2 separate log files.
    globalArgs.fullMsgLogPath = "";
    globalArgs.fullErrLogPath = "";

    int opt = 0;
    int longIndex = 0;

    // Only parse the command line options if there were options after the program name.
    if ( argc > 1 )

    {
        do
        {

            opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
            switch( opt )
            {

                case 'p':
                    globalArgs.logPath = optarg;
                    break;

                case 'm':
                    globalArgs.msgLogName = optarg;
                    break;

                case 'e':
                    globalArgs.errLogName = optarg;
                    break;

                case 0:
                    // Long options without short equivalent.
                    if( strcmp( "logging", longOpts[longIndex].name ) == 0 )
                    {
                        globalArgs.logging = 1;
                    }
                    if( strcmp( "onelog", longOpts[longIndex].name ) == 0 )
                    {
                        globalArgs.oneLog = 1;
                    }
                    if( strcmp( "help", longOpts[longIndex].name ) == 0 )
                    {
                        display_usage( globalArgs.thisProgram );
                    }
                    break;

                case '?':
                    display_usage( globalArgs.thisProgram );
                    break;

                default:
                    // We better not get here.
                    break;

            }

        }
        while( opt != -1 );

        // Construct the full path to the log files.
        globalArgs.fullMsgLogPath = globalArgs.logPath + "/" + globalArgs.msgLogName;
        globalArgs.fullErrLogPath = globalArgs.logPath + "/" + globalArgs.errLogName;

        // Only when at least --logging was specified. Otherwise default behavior: no logs.
        if ( globalArgs.logging == 1 )
        {

            if ( globalArgs.oneLog != 1 ) // 2 separate logs?
            {

                fpMsgLog = freopen (globalArgs.fullMsgLogPath.c_str(),"a",stdout); // Returns a NULL pointer on failure.
                fpErrLog = freopen (globalArgs.fullErrLogPath.c_str(),"a",stderr);

            }
            else
            {

                fpMsgLog = freopen (globalArgs.fullMsgLogPath.c_str(),"a",stdout);
                fpErrLog = freopen (globalArgs.fullMsgLogPath.c_str(),"a",stderr);

            }

        }

    }
// end   RW001

  read_hostnames();

  if(!selfpipe) {
    fout << "Could not set up self-pipe." << endl;
    return 1;
  }
  selfpipe.catchsig(SIGCHLD);

  if(!open_trigger())
    return 1;
  if(chdir(QUEUE_MSG_DIR) == -1) {
    fout << "Could not chdir to queue message directory." << endl;
    return 1;
  }

  signal(SIGALRM, catch_alrm);
  signal(SIGHUP, SIG_IGN);
  load_config();
  load_files();
  for(;;) {
    send_all();
    if (minpause == 0) break;
    do_select();
  }
// begin RW001
  if (fpMsgLog) { fclose (fpMsgLog); }
  if (fpErrLog) { fclose (fpErrLog); }
// end   RW001
  return 0;
}
